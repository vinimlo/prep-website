/*======================================================
# MENU
======================================================*/

const body = document.body;
const tagI = document.querySelector('.hamburger i');
const hamburger = document.querySelector('.hamburger');
const navLinks = document.querySelector('.nav-links');
const links = document.querySelectorAll('.nav-links .link');

function dropdownMenu () {
    body.classList.toggle('overflow'); // enable or disable the overflow class on css

    tagI.classList.toggle('change-color');

    navLinks.classList.toggle('open'); // enable or disable the open class on css

    links.forEach(link => { // for each element of links
        link.classList.toggle('fade'); // enable or disable the fade class on css
    })
}

hamburger.addEventListener('click', dropdownMenu);

links.forEach(link => {
    link.addEventListener('click', dropdownMenu);
})

/*======================================================
# ÍCONES INTERATIVOS
======================================================*/

const icons = document.querySelectorAll('.like');

icons.forEach(icon => {
    icon.addEventListener('click', () => {
        icon.classList.toggle('liked');
    })
});

/*======================================================
# DATA PARA O FOOTER
======================================================*/

var data = new Date();
var ano = data.getFullYear();

if (ano > 2021)
{
    document.getElementById('date').innerText = "2021 - " + ano;
}
else
{
    document.getElementById('date').innerText = ano;
}
