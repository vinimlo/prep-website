# Prep-Olimpiadas Website

Para servir a aplicação, é preciso ter Python e pip instalado na máquina.

Sugestão: Execute um ambiente virtual para não poluir a máquina com pacotes desnecessários. 

Para tal, crie o ambiente:

```
python3 -m venv .env
```

Ative o ambiente:

```
source .env/bin/activate
```

Para instalar o Flask, rode:

```
pip install flask
```

Mude a variável FLASK_ENV para o modo desenvolvimento:

```
export FLASK_ENV=development
```

Execute a aplicação:

```
flask run
```
