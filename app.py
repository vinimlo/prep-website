from pathlib import Path

from flask import Flask, render_template, request, redirect, url_for
from flask_frozen import Freezer
import os
import json

file = open('olimpiadas.json',)
olympiad_list = json.load(file)

app = Flask(__name__)
app.config['FREEZER_BASE_URL'] = 'https://vinimlo.gitlab.io/prep-website/'
app.config['FREEZER_DESTINATION'] = 'public'
freezer = Freezer(app)

@app.cli.command()
def freeze():
    freezer.freeze()

@app.cli.command()
def serve():
    freezer.run()

@freezer.register_generator
def page_generator():
    for template_path in app.jinja_env.list_templates():
        try:
            page = Path(template_path).relative_to("content").stem
            yield 'pages', {'page': page}
        except ValueError:
            pass

@app.route("/")
@app.route("/index.html")
def index():
    return render_template('index.html'), 200

@app.route("/olimpiadas.html")
def olimpiadas():
    return render_template('olimpiadas.html', olympiad_list=olympiad_list), 200

@app.route("/info-olimpiada.html", methods=['GET'])
def info_olimpiada():
    return render_template('info-olimpiada.html', name=request.args.get('name'), area=request.args.get('area'), date=request.args.get('date'), description=request.args.get('description'), image=request.args.get('image')), 200
